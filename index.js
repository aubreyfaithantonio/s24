// console.log("Hello");

// Using Exponent Operator
const num1 = 2;
const getCube = num1**3;

console.log(`The cube of ${num1} is ${getCube}`);

// Destructuring Array
const address = ['258', 'Washington Ave NW', 'California', '90011'];
const [house_number, street_name, city_name, zip_code] = address
console.log(`I live at ${house_number} ${street_name}, ${city_name} ${zip_code}`)

// Destrcuturing Object
const animal = {
	given_name: "Lolong",
	animal_type: "saltwater crocodile",
	weight: '1075 kgs',
	measurement: '20 ft 3 in'
};

const {given_name, animal_type, weight, measurement} = animal;
console.log(`${given_name} was a ${animal_type}. He weighed at ${weight} with a measurement of ${measurement}.`);

// Using Arrow Function for loops
const numbers = [1,2,3,4,5]
numbers.forEach((number) => console.log(number));

// Using Arrow function for reduce array method
const reduce_number = numbers.reduce((num1, num2) => {
	return num1 + num2
})

console.log(reduce_number);